import os
import pyinotify

from dotenv import load_dotenv
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

# Load env variables
load_dotenv()
watcher_path = os.getenv('WATCHER_PATH', './sync/')
delete_uploaded = (os.getenv('DELETE_UPLOADED', 'False'))

# Google Drive Authentication
gauth = GoogleAuth()
gauth.CommandLineAuth()

drive = GoogleDrive(gauth)


def upload_file(filename, path):
    try:
        file = drive.CreateFile({'title': filename})
        file.SetContentFile(path)
        print("Uploading file", filename)
        file.Upload()
        print(filename, "Uploaded!")
        
        if delete_uploaded == 'True':
            delete_file(path)
    except:
        print("Error uploading the file:", filename)

def delete_file(path):
    if os.path.exists(path):
        os.remove(path)
    else:
        print("The file {0} does not exist".format(path))


class EventHandler(pyinotify.ProcessEvent):
    def process_IN_CREATE(self, event):
        print("Creating:", event.pathname)

    def process_IN_DELETE(self, event):
        print("Removing:", event.pathname)

    def process_IN_ATTRIB(self, event):
        upload_file(event.name, watcher_path + event.name)


wm = pyinotify.WatchManager()  # Watch Manager
mask = pyinotify.IN_ATTRIB | pyinotify.IN_CREATE | pyinotify.IN_DELETE

handler = EventHandler()
notifier = pyinotify.Notifier(wm, handler)
wdd = wm.add_watch(watcher_path, mask, rec=True)

notifier.loop()
