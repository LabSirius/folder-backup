# folder-backup
Sync folder from server with Google Drive

## ⚠️ Notes

* 

## Requirements

* Python >= 3.5

## Settings

### Environment Variables

* [python-dotenv](https://github.com/theskumar/python-dotenv)

Rename the file `.env.example` to `.env`

| Name | Description | Default Value |
| - | - | - |
| `WATCHER_PATH` | Path of folder watched | `./sync/` |
| `DELETE_UPLOADED` | Enable delete files after upload | `False` |

### PyDrive

* [Reference](https://medium.com/@annissouames99/how-to-upload-files-automatically-to-drive-with-python-ee19bb13dda)

#### settings.yml

Authentication instructions ([Reference](https://gsuitedevs.github.io/PyDrive/docs/build/html/oauth.html))

```yaml
client_config_backend: settings
client_config:
  client_id: 9637341109347.apps.googleusercontent.com
  client_secret: psDskOoWr1P602PXRTHi

save_credentials: True
save_credentials_backend: file
save_credentials_file: credentials.json

get_refresh_token: True

oauth_scope:
  - https://www.googleapis.com/auth/drive.file
  - https://www.googleapis.com/auth/drive.install

```

## Run

Install dependencies

```
pip install -r requirements.txt
```

Run script

```
python main.py
```